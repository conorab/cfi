# Change Log

## Version 6

Last version which is part of the initial commits. Previous versions should not be used.

## Version 5

No notes.

## Version 4

No notes.

## Version 3

No notes.

## Version 2

No notes.

## Version 1

No notes.

## Initial notes

Versions 1-6 (shown in the quote as 'cfi.bsh') were made quite a while ago. The following terminal output indicates the last modification date and SHA-256 checksums.

	root@wind-tunnel:/disks/jwtk# stat cfi-v1.bsh 
	  File: cfi-v1.bsh
	  Size: 6416      	Blocks: 14         IO Block: 6656   regular file
	Device: 31h/49d	Inode: 65709       Links: 1
	Access: (0660/-rw-rw----)  Uid: (    0/    root)   Gid: (    0/    root)
	Access: 2021-04-13 23:24:44.150778549 +1000
	Modify: 2019-12-26 02:38:49.647076245 +1100
	Change: 2020-09-06 15:41:53.358213708 +1000
	 Birth: -
	root@wind-tunnel:/disks/jwtk# stat cfi-v2.bsh 
	  File: cfi-v2.bsh
	  Size: 6551      	Blocks: 14         IO Block: 6656   regular file
	Device: 31h/49d	Inode: 65710       Links: 1
	Access: (0660/-rw-rw----)  Uid: (    0/    root)   Gid: (    0/    root)
	Access: 2021-04-13 23:24:50.522804503 +1000
	Modify: 2019-11-23 11:12:25.099687414 +1100
	Change: 2020-09-06 15:41:41.950178064 +1000
	 Birth: -
	root@wind-tunnel:/disks/jwtk# stat cfi-v3.bsh 
	  File: cfi-v3.bsh
	  Size: 9548      	Blocks: 11         IO Block: 9728   regular file
	Device: 31h/49d	Inode: 33160       Links: 1
	Access: (0660/-rw-rw----)  Uid: (    0/    root)   Gid: (    0/    root)
	Access: 2021-04-13 23:24:56.310828071 +1000
	Modify: 2019-12-26 02:29:33.569466162 +1100
	Change: 2020-09-06 15:41:53.358213708 +1000
	 Birth: -
	root@wind-tunnel:/disks/jwtk# stat cfi-v4.bsh 
	  File: cfi-v4.bsh
	  Size: 10392     	Blocks: 11         IO Block: 10752  regular file
	Device: 31h/49d	Inode: 32776       Links: 1
	Access: (0660/-rw-rw----)  Uid: (    0/    root)   Gid: (    0/    root)
	Access: 2021-04-13 23:25:03.562857592 +1000
	Modify: 2020-03-07 13:21:07.278044081 +1100
	Change: 2020-09-06 15:41:53.354213695 +1000
	 Birth: -
	root@wind-tunnel:/disks/jwtk# stat cfi-v5.bsh 
	  File: cfi-v5.bsh
	  Size: 11201     	Blocks: 23         IO Block: 11264  regular file
	Device: 31h/49d	Inode: 65670       Links: 1
	Access: (0751/-rwxr-x--x)  Uid: ( 1000/ conorab)   Gid: ( 1000/ conorab)
	Access: 2021-04-13 23:26:26.451194326 +1000
	Modify: 2020-10-21 19:09:29.514311024 +1100
	Change: 2021-04-13 23:25:49.995046370 +1000
	 Birth: -
	root@wind-tunnel:/disks/jwtk# stat cfi.bsh 
	  File: cfi.bsh
	  Size: 13510     	Blocks: 28         IO Block: 13824  regular file
	Device: 31h/49d	Inode: 65686       Links: 1
	Access: (0761/-rwxrw---x)  Uid: (    0/    root)   Gid: (    0/    root)
	Access: 2021-04-13 23:25:16.098908597 +1000
	Modify: 2020-10-21 19:43:27.133565000 +1100
	Change: 2021-03-25 10:03:51.307364625 +1100
	 Birth: -

	ee3fa9dfd3792278824c00c6e066dc06e15de535884f46e9b0026c2e526c5a84  cfi-v1.bsh
	6b9a3d781fdbf0320a610b639744e1632f7adcf0482f3ec4f06a98874dd45b66  cfi-v2.bsh
	9b8e29ec5850d2cf285d23f4651a9f0ae91ef65be7d70fb76408c5b4bb880c9d  cfi-v3.bsh
	9f7f00905c8f178212136711980113e7d9d6c65ea4a59549650bb19f3fe617a4  cfi-v4.bsh
	a16210a63f8dae444128e7aebcd49b20ad452d3e16a134e5a5cfd109677996e6  cfi-v5.bsh
	17128166b4bd1d172c23d836ed60a717221b0253b7f4a4db0cd16e4ce8d60f5c  cfi.bsh
