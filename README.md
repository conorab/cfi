# Conor's File Index

CFI (Conor's File Index) is a script used to keep a file-level de-duplicated archive of files. Each file is stored in a 'data' folder while the file structure is maintained via symbolic links to the 'data' folder. If a file matching the SHA sum of an existing file is added, the new file is discarded and a symbolic link is instead made to the original file thus achieving de-duplication. Each file is also tracked by it's SHA sum which also means the integrity of each file can be verified after the fact.

The main purpose of this program is to allow files to be archived and referenced over the long term even if the path (symbolic link) changes, as you can refer to the name of the file in the 'data' folder instead. Integrity of the data set as a whole over time can also be maintained by storing the SHA sums of the data files to ensure they have not changed.
